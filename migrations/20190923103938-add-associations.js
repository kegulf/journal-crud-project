"use strict";

module.exports = {
	up: async (queryInterface, Sequelize) => {
		await queryInterface.addColumn("JournalEntries", "ownerId", {
			type: Sequelize.INTEGER,
			refrences: {
				model: "Users",
				key: "id"
			}
		});
	},

	down: (queryInterface, Sequelize) => {
		return queryInterface.removeColumn("JournalEntries", "ownerId");
	}
};
