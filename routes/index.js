const journalEntryController = require("../controllers").journalEntryController;
const userController = require("../controllers").userController;
const authController = require("../controllers").authController;
const LocalStrategy = require("passport-local").Strategy;

const path = require("path");

const express = require("express");
const router = express.Router();

const morgan = require("morgan");
const bodyParser = require("body-parser");

const session = require("express-session");

const app = express();

app.use(morgan("tiny"));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

app.use(
	session({
		resave: true,
		saveUninitialized: true,
		secret: "keyboard cat",
		cookie: {
			httpOnly: true,
			maxAge: 3600000 // 1 hour
		}
	})
);

require("./setupPassport")(app);

const jwt = require("jsonwebtoken");
const passport = require("passport");
//require("../config/passport")(passport);

// /* GET home page. */
// router.get("/", function(req, res, next) {
// 	res.render("index", { title: "Express/Homepage" });
// });

/* GET dashboard. when authenticated. */
// router.get("/dashboard", isAuthenticated, function(req, res) {
// 	res.render("dashboard");
// });

/* JournalEntry Router */
router.get("/api/journal-entries/all", journalEntryController.list);
router.get("/api/journal-entries/:id", journalEntryController.getById);
router.post("/api/journal-entries/add", journalEntryController.add);
router.put("/api/journal-entries/:id", journalEntryController.update);
router.delete("/api/journal-entries/:id", journalEntryController.delete);

// /* User Router */
router.get("/api/users/all", userController.list);
router.get("/api/users/:id", userController.getById);
router.post("/api/users", userController.add);
router.put("/api/users/:id", userController.update);
router.delete("/api/users/:id", userController.delete);
router.get("/api/users/:userId/entries", userController.listJournalEntries);

// /* Authentication Router */
router.post("/api/signup", authController.signup);
//can also just use passport.authenticate("local")
router.post("/api/isAuthenticated", authController.isAuthenticated);
router.post("/api/login", passport.authenticate("local"), authController.login);
router.get("/api/logout", authController.logout);

app.use(router);

if (process.env.NODE_ENV === "production") {
	// When in production mode: serve the built webpack app
	// When in development: the API is proxied from the create-react-app server.
	app.use(express.static(path.join(process.cwd(), "app", "build")));
}

// Default error handler
// If you ever see this then you've done something really wrong.
app.use((err, req, res, next) => {
	console.error(err);

	// Send error message to client while not in production mode
	if (process.env.NODE_ENV !== "production") {
		return res.status(500).json({ status: "error", error: err });
	}

	res.status(500).json({ status: "error" });
});

const port = process.env.PORT || 3000;

const server = app.listen(port, function() {
	console.log("express is now listening on port 3001");
});

module.exports = router;
