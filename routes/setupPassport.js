var passport = require("passport"),
	LocalStrategy = require("passport-local").Strategy,
	bcrypt = require("bcrypt"),
	db = require("../models");

//MAY HAVE TO SET THIS UP IN INDEX.JS OR FIND AWAY TO EXPORT IT INTO INDEX.JS

//may have to use
// const app = express();

module.exports = function(app) {
	app.use(passport.initialize());
	app.use(passport.session());

	passport.serializeUser((user, done) => done(null, user.id || user));

	passport.deserializeUser(async (userId, done) => {
		try {
			const user = await db.User.findByPk(userId);

			done(null, user);
		} catch (err) {
			done(err, null);
		}
	});

	passport.use(
		new LocalStrategy(
			{
				usernameField: "email",
				passwordField: "password",

				session: true,
				passReqToCallback: true
			},
			async (req, username, password, done) => {
				try {
					const store = await db.User.findOne({
						where: { email: req.body.email }
					});

					if (!store) {
						done(null, false, { message: "User not found" });
					}

					const result = await new Promise((resolve, reject) => {
						bcrypt.compare(password, store.password, (err, success) => {
							if (err) {
								return reject(err);
							}

							return resolve(success);
						});
					});

					if (result) {
						return done(null, store);
					} else {
						return done(null, false, { message: "Inncorrect password" });
					}
				} catch (err) {
					if (err.notFound) {
						console.log("User don't exsist");
						return done(null, false, { message: "User don't exsist" });
					}

					console.log("something went really to s**t");
					return done(err);
				}
			}
		)
	);
};
