"use strict";
module.exports = (sequelize, DataTypes) => {
	const JournalEntry = sequelize.define(
		"JournalEntry",
		{
			title: DataTypes.STRING,
			subtitle: DataTypes.STRING,
			text: DataTypes.STRING
		},
		{}
	);
	JournalEntry.associate = function(models) {
		// associations can be defined here
		JournalEntry.belongsTo(models.User, {
			foreignKey: "ownerId"
		});
	};
	return JournalEntry;
};
