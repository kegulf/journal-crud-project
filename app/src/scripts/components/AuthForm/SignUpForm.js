import Modal from "./../Modal/Modal";

const SignUpForm = props => {
	const { onSignUpButtonClick } = props;

	return Modal({
		id: "sign-up-form",
		headline: "Sign up as new user",
		children: [
			`<p>Pleeease sign up...</p>`,
			`<p>What are you waiting for??!! </p>`
		],
		primaryButtonText: "Sign up",
		secondaryButtonText: "Cancel",
		primaryButtonOnClick: () => onSignUpButtonClick
	});
};

export default SignUpForm;
