import $ from "jquery";

import { logout } from "./../../dataHandler/authHandler";

const Toolbar = props => {
	const { userLoggedIn, type, title, addButton, color } = props;

	if (type === "top") {
		const linkList = $(`<ul class="navbar-nav navbar-expand mr-auto"></ul>`);

		const li = $(`<li class="mr-3 nav-item"></li>`);

		if (!userLoggedIn) {
			const loginAnchor = $(`
			<a 
				class="nav-link" 
				href="#" 
				data-toggle="modal" 
				data-target="#login-modal">Log in</a>`);

			loginAnchor.on("click", props.showLoginModal);

			li.append(loginAnchor);
			linkList.append(li);

			const signupAnchor = $(`
			<a 
				class="nav-link" 
				href="#" 
				data-toggle="modal" 
				data-target="#signup-modal">Sign up</a>`);

			signupAnchor.on("click", props.showSignupModal);

			const signupLi = $(`<li class="nav-item"></li>`);

			signupLi.append(signupAnchor);
			linkList.append(signupLi);
		} else {
			const logoutAnchor = $(`<a class="nav-link" href="#">Log out</a>`);
			logoutAnchor.on("click", logout);
			li.append(logoutAnchor);
			linkList.append(li);
		}

		const toolbar = $(`
			<nav 
				class="navbar absolute-top navbar-light" 
				style=" background-color: ${color};">
		
				<a class="navbar-brand" href="#">${title}</a>
				
			</nav>`);

		toolbar.append(linkList);
		toolbar.append(addButton);

		return toolbar;
	} else if (type === "bottom") {
		return `
			<nav 
				class="navbar fixed-bottom navbar-light" 
				style="height: 30px; background-color: ${color};">
			</nav>
    `;
	}
};

export default Toolbar;
