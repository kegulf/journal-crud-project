import { Modal } from "./..";
import { InputField } from "../../controls";

const LoginModal = props => {
	const loginModal = Modal({
		modalId: "login-modal",
		modalType: "primary",
		headline: "Login",
		children: createChildrenComponents(),
		primaryButtonText: "Log in",
		secondaryButtonText: "Cancel",
		primaryButtonOnClick: props.onLoginButtonClick
	});

	return loginModal;
};

const createChildrenComponents = () => {
	const emailInput = InputField({
		id: "email-input",
		type: "email",
		labelText: "Email:",
		placeholder: "Email",
		value: ""
	});

	const passwordInput = InputField({
		id: "password-input",
		type: "password",
		labelText: "Password:",
		placeholder: "Password",
		value: ""
	});
	return [emailInput, passwordInput];
};

export default LoginModal;
