import { Modal } from "./..";
import { InputField } from "../../controls";

const SignupModal = props => {
	const signupModal = Modal({
		modalId: "signup-modal",
		modalType: "primary",
		headline: "Signup",
		children: createChildrenComponents(),
		primaryButtonText: "Sign up",
		secondaryButtonText: "Cancel",
		primaryButtonOnClick: props.onSignupButtonClick
	});

	return signupModal;
};

const createChildrenComponents = () => {
	const firstnameInput = InputField({
		id: "firstname-input",
		type: "text",
		labelText: "Firstname:",
		placeholder: "Firstname",
		value: ""
	});

	const lastnameInput = InputField({
		id: "lastname-input",
		type: "text",
		labelText: "Lastname:",
		placeholder: "Lastname",
		value: ""
	});

	const emailInput = InputField({
		id: "email-input",
		type: "text",
		labelText: "Email:",
		placeholder: "Email",
		value: ""
	});

	const passwordInput = InputField({
		id: "password-input",
		type: "password",
		labelText: "Password:",
		placeholder: "Password",
		value: ""
	});

	return [firstnameInput, lastnameInput, emailInput, passwordInput];
};

export default SignupModal;
