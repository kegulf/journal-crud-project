import JournalFormModal from "./JournalFormModal/JournalFormModal";
import Modal from "./Modal/Modal";
import DeleteEntryModal from "./DeleteEntryModal/DeleteEntryModal";
import SignupModal from "./SignupModal/SignupModal";
import LoginModal from "./LoginModal/LoginModal";

export { Modal, JournalFormModal, DeleteEntryModal, SignupModal, LoginModal };
