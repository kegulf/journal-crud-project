import { Modal } from "./../";

const DeleteEntryModal = props => {
	const { onConfirmDeleteButtonClick, entry } = props;

	return Modal({
		modalId: "delete-entry-modal",
		modalType: "danger",
		headline: "Please confirm action",
		children: [
			`<p>Delete opartions are final and can't be undone.`,
			`<p>Are you sure you want to delete ${entry.title}? </p>`
		],
		primaryButtonText: "Delete",
		secondaryButtonText: "Cancel",
		primaryButtonOnClick: () => onConfirmDeleteButtonClick(entry.id)
	});
};

export default DeleteEntryModal;
