import $ from "jquery";

import PropTypes from "prop-types";

import { Modal } from "./../";
import { InputField, TextArea } from "./../../controls/";

const JournalForm = props => {
	// The prop entryDetails will be the state object sent in by the FormFeedPage.
	const { modalId, modalType, isNewEntry, onSaveButtonClick, entryDetails } = props;

	const saveButtonOnClick = () => {
		onSaveButtonClick(entryDetails.id, {
			ownerId: entryDetails.ownerId,
			title: $(`#entry-title`).val(),
			subtitle: $(`#entry-subtitle`).val(),
			text: $(`#entry-text`).val()
		});
	};

	const journalForm = Modal({
		modalId,
		modalType: modalType || null,
		headline: isNewEntry ? "Create new Entry" : "Edit entry",
		children: createFormInputControls(entryDetails),
		primaryButtonText: "Save changes",
		secondaryButtonText: "Cancel",
		primaryButtonOnClick: saveButtonOnClick
	});

	return journalForm;
};

const createFormInputControls = entryDetails => {
	const entryTitleInputField = InputField({
		id: "entry-title",
		type: "text",
		labelText: "Entry title:",
		placeholder: "Title",
		value: entryDetails.title || ""
	});

	const entrySubtitleInputField = InputField({
		id: "entry-subtitle",
		type: "text",
		labelText: "Entry subtitle:",
		placeholder: "Subtitle",
		value: entryDetails.subtitle || ""
	});

	const entryBodyTextArea = TextArea({
		id: "entry-text",
		labelText: "Entry text:",
		value: entryDetails.text || ""
	});

	return [entryTitleInputField, entrySubtitleInputField, entryBodyTextArea];
};

JournalForm.propTypes = {
	modalId: PropTypes.string.isRequired,
	modalType: PropTypes.string, // default primary
	isNewEntry: PropTypes.bool.isRequired,
	setFormInputValues: PropTypes.func.isRequired,
	entryDetails: PropTypes.object
};

export default JournalForm;
