import $ from "jquery";

import PropTypes from "prop-types";

const ModalBody = props => {
	const { children } = props;

	const modalBody = $(`<div class="modal-body"></div>`);

	children.forEach(child => {
		modalBody.append(child);
	});

	return modalBody;
};

ModalBody.propTypes = {
	children: PropTypes.array
};

export default ModalBody;
