import $ from "jquery";

import PropTypes from "prop-types";

const ModalHeader = props => {
	const { headline, modalType } = props;
	return $(`
        <div class="modal-header ">
            <h5 class="modal-title " id="entry-form-title">
                ${props.headline}
            </h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>`);
};

ModalHeader.propTypes = {
	modalType: PropTypes.string,
	headline: PropTypes.string.isRequired
};

export default ModalHeader;
