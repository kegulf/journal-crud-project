import $ from "jquery";

import ModalHeader from "./ModalHeader";
import ModalBody from "./ModalBody";
import ModalFooter from "./ModalFooter";

import PropTypes from "prop-types";

const Modal = props => {
	const {
		modalId,
		modalType,
		headline,
		children,
		primaryButtonText,
		secondaryButtonText,
		primaryButtonOnClick
	} = props;

	const modal = $(`
    <div
            class="modal fade"
            id="${modalId}"
            tabindex="-1"
            role="dialog"
            aria-labelledby=${modalId}
            aria-hidden="true"></div>`);

	const modalDialog = $(`
        <div 
            class="modal-dialog" 
            role="document">
        </div>
        `);

	const modalContent = $(`<div class="modal-content"></div>`);

	modalContent.append(ModalHeader({ modalType, headline }));
	modalContent.append(ModalBody({ children }));
	modalContent.append(
		ModalFooter({
			modalType,
			primaryButtonText,
			secondaryButtonText,
			primaryButtonOnClick
		})
	);

	modalDialog.append(modalContent);
	modal.append(modalDialog);

	return modal;
};

Modal.propTypes = {
	modalId: PropTypes.string.isRequired,
	moodalType: PropTypes.string,
	headline: PropTypes.string.isRequired,
	children: PropTypes.array,
	primaryButtonText: PropTypes.string.isRequired,
	secondaryButtonText: PropTypes.string.isRequired,
	primaryButtonOnClick: PropTypes.func.isRequired
};

export default Modal;
