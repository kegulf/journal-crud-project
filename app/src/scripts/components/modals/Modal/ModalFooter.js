import $ from "jquery";

import PropTypes from "prop-types";

const ModalFooter = props => {
	const { modalType, primaryButtonText, secondaryButtonText, primaryButtonOnClick } = props;

	const modalFooter = $(`
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">
                ${secondaryButtonText}
            </button>   
        </div>`);

	const primaryButton = $(`
        <button type="button" class="btn btn-${modalType || "primary"}" data-dismiss="modal">
            ${primaryButtonText}
        </button>`);

	primaryButton.on("click", primaryButtonOnClick);

	modalFooter.append(primaryButton);

	return modalFooter;
};

ModalFooter.propTypes = {
	modalType: PropTypes.string,
	primaryButtonText: PropTypes.string.isRequired,
	secondaryButtonText: PropTypes.string.isRequired,
	primaryButtonOnClick: PropTypes.func.isRequired
};

export default ModalFooter;
