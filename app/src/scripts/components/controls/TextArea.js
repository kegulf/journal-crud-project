const TextArea = props => {
	const { id, value, labelText } = props;
	return `<div class="form-group">
            <label for="entry-body">${labelText}</label>
            <textarea 
                class="form-control" 
                name=${id} 
                id=${id} 
                rows="3"
                >${value}</textarea>
        </div>`;
};

export default TextArea;
