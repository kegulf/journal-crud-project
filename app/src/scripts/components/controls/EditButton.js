import $ from "jquery";

const EditButton = props => {
	const { editButtonTarget, editButtonOnClick, entry } = props;

	const editButton = $(`
		<button 
			type="button" 
			class="btn btn-outline-secondary"
			data-toggle="modal" 
			data-target="${editButtonTarget}">
				<i class="fas fa-edit"></i>
		</button>`);

	editButton.on("click", () => {
		editButtonOnClick(null, entry.id, entry);
	});

	return editButton;
};
export default EditButton;
