import PropTypes from "prop-types";

/**
 * PropTypes:
 * - id
 * - type
 * - labelText
 * - placeholder
 * - value
 * @param {*} props
 */

const InputField = props => {
	const { id, type, labelText, placeholder, value } = props;

	return `<div class="form-group">
                <label for=${id}>${labelText}</label>
                <input 
                    type=${type} 
                    class="form-control" 
                    id=${id} 
                    name=${id} 
                    placeholder=${placeholder}
                    value="${value}">
            </div>`;
};

InputField.propTypes = {
	id: PropTypes.string.isRequired,
	type: PropTypes.string.isRequired,
	labelText: PropTypes.string.isRequired,
	placeholder: PropTypes.string.isRequired,
	value: PropTypes.string.isRequired
};

export default InputField;
