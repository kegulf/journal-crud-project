import $ from "jquery";

const LoginButton = props => {
	const loginButton = $(`
        <button 
            class="btn btn-primary m-1" 
            style="width: 100px"
            data-toggle="modal" 
            data-target="#login-modal"}>
            Login
        </button>`);

	loginButton.on("click", props.onClick);

	return loginButton;
};

export default LoginButton;
