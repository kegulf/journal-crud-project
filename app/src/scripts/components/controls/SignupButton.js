import $ from "jquery";

const SignupButton = props => {
	const signupButton = $(`
        <button 
            class="btn btn-primary m-1 "
            style="width: 100px"
            data-toggle="modal" 
            data-target="#signup-modal">
            Sign up
        </button>`);

	signupButton.on("click", props.onClick);

	return signupButton;
};

export default SignupButton;
