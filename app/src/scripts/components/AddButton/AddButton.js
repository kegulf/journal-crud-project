import $ from "jquery";

import "@fortawesome/fontawesome-free";
import "@fortawesome/fontawesome-free/css/all.css";

const AddButton = props => {
	const { type, color, target, onclick } = props;

	if (type === "contained") {
		const button = $(`
			<button class="btn btn-primary"
				type="button"
				style="background-color: ${color};"
				data-toggle="modal" 
				data-target=${target}>
				
				<i class="fas fa-plus"></i>
			</button>`);

		button.on("click", onclick);
		return button;
	} else if (type === "fab") {
		const button = $(`
		<button class="btn btn-primary"
			type="button"
			style="position: fixed; z-index: 9999; bottom: 2%; right: 5%; background-color: ${color}"
			data-toggle="modal" 
			data-target="${target}">

			<i class="fas fa-plus"></i>
		</button>`);

		button.on("click", onclick);
		return button;
	}
};

export default AddButton;
