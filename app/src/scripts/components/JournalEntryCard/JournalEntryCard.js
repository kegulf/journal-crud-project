import $ from "jquery";

import moment from "moment";
import "@fortawesome/fontawesome-free/css/all.css";
import EditButton from "../controls/EditButton";
import DeleteButton from "../controls/DeleteButton";

const JournalEntryCard = props => {
	const {
		editButtonTarget,
		editButtonOnClick,
		deleteButtonTarget,
		deleteButtonOnClick,
		entry
	} = props;

	const { id, title, subtitle, createdAt, updatedAt, text } = entry;

	const cardHeader = $(`<div class="d-flex w-100 justify-content-between"></div>`);
	cardHeader.append(`<h4 class="">${title}</h4>`);

	const buttonGroup = $(`<div class="btn-group" role="toolbar"></div>`);

	const editButton = EditButton({ editButtonTarget, editButtonOnClick, entry });
	buttonGroup.append(editButton);

	const deleteButton = DeleteButton({ deleteButtonTarget, deleteButtonOnClick, entry });
	buttonGroup.append(deleteButton);

	cardHeader.append(buttonGroup);

	const cardWrapper = $(
		`<a key="${id} value="${id}" "href="#" class="mt-2 mb-2 list-group-item list-group-item-action"></a>`
	);
	cardWrapper.append(cardHeader);
	cardWrapper.append(`
		<p class="m-0"><strong>${subtitle}</strong></p>
		<small class="text-muted">${
			updatedAt != createdAt
				? "Edited: " + moment(updatedAt).format("DD. MMM YYYY HH:mm:SS")
				: "Created: " + moment(createdAt).format("DD. MMM YYYY HH:mm:SS")
		}</small>
		<p class="mb-1 mt-2">${text}</p>`);

	return cardWrapper;
};

export default JournalEntryCard;
