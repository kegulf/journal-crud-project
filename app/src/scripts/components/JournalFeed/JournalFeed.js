import JournalEntryCard from "./../JournalEntryCard/JournalEntryCard";

import $ from "jquery";

const JournalFeed = props => {
	const {
		editButtonTarget,
		editButtonOnClick,
		deleteButtonTarget,
		deleteButtonOnClick,
		entries
	} = props;

	if (entries && Array.isArray(entries)) {
		const entryCards = entries.map(entry => {
			return JournalEntryCard({
				editButtonTarget,
				editButtonOnClick,
				deleteButtonTarget,
				deleteButtonOnClick,
				entry
			});
		});

		const journalFeed = $("<section></section>");

		entryCards.forEach(card => {
			journalFeed.append(card);
		});

		return journalFeed;
	}

	return $("<h4>Loading</h4>");
};

export default JournalFeed;
