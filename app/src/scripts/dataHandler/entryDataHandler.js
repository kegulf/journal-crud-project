export const createJournalEntry = entry => {
	console.log(entry);

	return fetch(`/api/journal-entries/add`, {
		method: "POST",
		mode: "cors",
		cache: "no-cache",
		credentials: "same-origin",
		headers: {
			"Content-Type": "application/json"
		},
		body: JSON.stringify(entry)
	}).then(result => {
		handleErrors(result.clone());
		return result.json();
	});
};

export const getJournalEntry = id => {
	return fetch(`/api/journal-entries/${id}`, {
		method: "GET",
		mode: "cors",
		credentials: "same-origin",
		headers: {
			"Content-Type": "application/json"
		}
	}).then(result => {
		handleErrors(result.clone());
		return result.json();
	});
};

export const getUserEntries = userId => {
	return fetch(`/api/users/${userId}/entries`, {
		method: "GET",
		mode: "cors",
		cache: "no-cache",
		credentials: "same-origin",
		headers: {
			"Content-Type": "application/json"
		}
	}).then(result => {
		handleErrors(result.clone());
		return result.json();
	});
};

export const updateJournalEntry = (id, entry) => {
	return fetch(`/api/journal-entries/${id}`, {
		method: "PUT",
		mode: "cors",
		cache: "no-cache",
		credentials: "same-origin",
		headers: {
			"Content-Type": "application/json"
		},
		body: JSON.stringify(entry)
	}).then(result => {
		handleErrors(result.clone());
		return result.json();
	});
};

export const deleteJournalEntry = id => {
	return fetch(`/api/journal-entries/${id}`, {
		method: "DELETE",
		mode: "cors",
		cache: "no-cache",
		credentials: "same-origin",
		headers: {
			"Content-Type": "application/json"
		}
	}).then(result => handleErrors(result.clone()));
};

const handleErrors = result => {
	if (!result.ok) {
		throw new Error("An error occured");
	}
};
