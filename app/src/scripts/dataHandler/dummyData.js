import * as moment from "moment";

export const dummyData = [
	{
		id: "1",
		title: "Test entry 1",
		subtitle: "A small test",
		created: "20190831",
		modified: "20190916",
		body:
			"Lorem ipsum dolor sit amet, consectetur adipiscing elit. In sed sagittis leo. Proin malesuada tellus in libero vestibulum scelerisque. Aliquam erat volutpat. Pellentesque efficitur, urna ultricies luctus pellentesque, magna massa pellentesque urna, ut malesuada velit metus a enim. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Phasellus egestas luctus vehicula. Donec porttitor feugiat lacus, tempus tincidunt metus. Aliquam convallis sed sapien eu venenatis. Aenean dapibus tempor ligula vel gravida. Integer mollis tellus ac ligula feugiat gravida sed vel risus. Cras consequat velit volutpat nisi aliquet dapibus. Pellentesque fermentum posuere nunc consequat molestie."
	},
	{
		id: "2",
		title: "Test entry 2",
		subtitle: "A small test",
		created: "20190731",
		modified: "20190823",
		body:
			"Lorem ipsum dolor sit amet, consectetur adipiscing elit. In sed sagittis leo. Proin malesuada tellus in libero vestibulum scelerisque. Aliquam erat volutpat. Pellentesque efficitur, urna ultricies luctus pellentesque, magna massa pellentesque urna, ut malesuada velit metus a enim. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Phasellus egestas luctus vehicula. Donec porttitor feugiat lacus, tempus tincidunt metus. Aliquam convallis sed sapien eu venenatis. Aenean dapibus tempor ligula vel gravida. Integer mollis tellus ac ligula feugiat gravida sed vel risus. Cras consequat velit volutpat nisi aliquet dapibus. Pellentesque fermentum posuere nunc consequat molestie."
	},
	{
		id: "3",
		title: "Test entry 3",
		subtitle: "A small test",
		created: "20190631",
		modified: "20190716",
		body:
			"Lorem ipsum dolor sit amet, consectetur adipiscing elit. In sed sagittis leo. Proin malesuada tellus in libero vestibulum scelerisque. Aliquam erat volutpat. Pellentesque efficitur, urna ultricies luctus pellentesque, magna massa pellentesque urna, ut malesuada velit metus a enim. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Phasellus egestas luctus vehicula. Donec porttitor feugiat lacus, tempus tincidunt metus. Aliquam convallis sed sapien eu venenatis. Aenean dapibus tempor ligula vel gravida. Integer mollis tellus ac ligula feugiat gravida sed vel risus. Cras consequat velit volutpat nisi aliquet dapibus. Pellentesque fermentum posuere nunc consequat molestie."
	}
];
