export const logout = () => {
	return fetch(`/api/logout`, {
		method: "GET",
		mode: "cors",
		cache: "no-cache",
		credentials: "same-origin",
		headers: {
			"Content-Type": "application/json"
		}
	}).then(result => {
		handleErrors(result.clone());
		document.location.reload(true);
	});
};

const handleErrors = result => {
	if (!result.ok) {
		throw new Error("An error occured");
	}
};
