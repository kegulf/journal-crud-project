import $ from "jquery";

import { LoginButton, SignupButton } from "./../components/controls";
import { LoginModal, SignupModal } from "./../components/modals";

const LoginPage = props => {
	const { onLoginButtonClick, onSignupButtonClick } = props;

	const loginPage = $(`<main></main>`);

	const buttonPanel = $(`<section class="text-center"></section>`);

	buttonPanel.append(
		LoginButton({
			onClick: () => showLoginModal(onLoginButtonClick)
		})
	);
	buttonPanel.append(
		SignupButton({
			onClick: () => showSignupModal(onSignupButtonClick)
		})
	);

	loginPage.append(buttonPanel);

	return loginPage;
};

export const showLoginModal = onLoginButtonClick => {
	$("#modal-container").empty();

	$("#modal-container").append(
		LoginModal({
			onLoginButtonClick
		})
	);
};
export const showSignupModal = onSignupButtonClick => {
	$("#modal-container").empty();

	$("#modal-container").append(
		SignupModal({
			onSignupButtonClick
		})
	);
};

export default LoginPage;
