import $ from "jquery";

import JournalFeed from "./../components/JournalFeed/JournalFeed";
import { JournalFormModal, DeleteEntryModal } from "./../components/modals";

import {
	createJournalEntry,
	getUserEntries,
	updateJournalEntry,
	deleteJournalEntry
} from "./../dataHandler/entryDataHandler";

// START JOURNAL ENTRIES STATE
const journalEntriesState = { entries: [] };

const setJournalEntriesState = updatedState => {
	journalEntriesState.entries = updatedState.entries;

	reRenderJournalFeed();
};

// END JOURNAL ENTRIES STATE

export const showJournalFormModal = (ownerId, entryId, entry) => {
	$("#modal-container").empty();

	let entryDetails = entryId ? entry : { ownerId };

	console.log(entryDetails);

	$("#modal-container").append(
		JournalFormModal({
			modalId: "journal-form-modal",
			isNewEntry: !entryId,
			onSaveButtonClick,
			entryDetails: entryDetails
		})
	);
};

const JournalMainPage = async props => {
	const entries = await getUserEntries(props.userId);

	setJournalEntriesState({ entries });
};

// -------------------------------------
//	START Create and Update Entry Logic
// -------------------------------------

const onSaveButtonClick = (id, entryInputValues) => {
	// write to DB here

	if (!id) {
		createJournalEntry(entryInputValues)
			.then(result => {
				setJournalEntriesState({
					entries: [result, ...journalEntriesState.entries]
				});
			})
			.catch(error => console.log(error));
	} else {
		updateJournalEntry(id, entryInputValues)
			.then(result => {
				setJournalEntriesState({
					entries: [result, ...journalEntriesState.entries.filter(entry => entry.id != id)]
				});
			})
			.catch(error => console.log(error));
	}
};

// -------------------------------------
//	END Create and Update Entry Logic
// -------------------------------------

// -------------------------------
//	START Delete Entry Logic
// -------------------------------

const showDeleteEntryModal = entry => {
	$("#modal-container").empty();

	$("#modal-container").append(
		DeleteEntryModal({
			entry,
			onConfirmDeleteButtonClick
		})
	);
};

const onConfirmDeleteButtonClick = id => {
	deleteJournalEntry(id)
		.then(result => {
			setJournalEntriesState({
				entries: journalEntriesState.entries.filter(entry => entry.id !== id)
			});
		})
		.catch(error => console.log("shit went to shit", error));
};

// -------------------------------
// 	END Delete Entry Logic
// -------------------------------

const reRenderJournalFeed = () => {
	$("#journal-entries-section").empty();

	$("#journal-entries-section").append(
		JournalFeed({
			editButtonTarget: "#journal-form-modal",
			editButtonOnClick: showJournalFormModal,
			deleteButtonTarget: "#delete-entry-modal",
			deleteButtonOnClick: showDeleteEntryModal,
			entries: journalEntriesState.entries
		})
	);
};

export default JournalMainPage;
