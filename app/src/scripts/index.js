import "../styles/index.scss";
import "bootstrap";
import "bootstrap/dist/css/bootstrap.min.css";

import $ from "jquery";

import Toolbar from "./components/Toolbar/Toolbar";
import AddButton from "./components/AddButton/AddButton";

import JournalMainPage, { showJournalFormModal } from "./pages/JournalMainPage";
import LoginPage, { showLoginModal, showSignupModal } from "./pages/LoginPage";

const postData = (url = "", data = {}) => {
	return fetch(url, {
		method: "POST",
		mode: "cors",
		cache: "no-cache",
		headers: {
			"Content-Type": "application/json"
		},
		redirect: "follow",
		referrer: "no-referrer",
		body: JSON.stringify(data)
	}).then(response => response.json());
};

const onSignupButtonClick = async e => {
	e.preventDefault();
	try {
		const result = await postData("/api/signup", getSignupInputs());
		console.log("REGISTER", result);

		if (result.status === "ok") {
			console.log(result.user);

			return setLoginState({ user: result.user, userEntries: null });
		} else if (result.status === "error") {
			console.log("Signup failed. " + result.error);
		}
	} catch (err) {
		console.error("ERROR", err);
		console.log("Signup failed. " + err.message);
	}

	setLoginState({ user: null, userEntries: [] });
};

const getSignupInputs = () => {
	return {
		name: $("#firstname-input").val() + " " + $("#lastname-input").val(),
		email: $("#email-input").val(),
		password: $("#password-input").val()
	};
};

// TO-DO implemnt
const onLoginButtonClick = async () => {
	console.log("trst");

	try {
		const result = await postData("/api/login", getLoginInputs());
		console.log("LOGIN", result);

		if (result.status === "ok") {
			console.log("Welcome user " + result.user.email);
			return setLoginState({ user: result.user, userEntries: result.entries });
		}
	} catch (err) {
		console.log("auth failed " + err);
	}

	setLoginState({ user: null, userEntries: [] });
};

const getLoginInputs = () => {
	return {
		email: $("#email-input").val(),
		password: $("#password-input").val()
	};
};

const loginState = { user: null };

const setLoginState = updatedState => {
	loginState.user = updatedState.user || loginState.user;
	rerenderApp();
};

$(document).ready(async () => {
	try {
		const response = await postData("/api/isAuthenticated", {});

		if (response.user) {
			setLoginState({ user: response.user });
		}
	} catch (err) {
		console.log(err);
	}
	rerenderApp();
});

const rerenderApp = () => {
	$("#toolbar").empty();

	$("#toolbar").append(
		Toolbar({
			showLoginModal,
			showSignupModal,
			userLoggedIn: loginState.user,
			type: "top",
			title: "Journal",
			addButton: !loginState.user
				? null
				: AddButton({
						type: "contained",
						color: "#1890ff",
						target: "#journal-form-modal",
						onclick: () => {
							showJournalFormModal(loginState.user.id);
						}
				  }),
			color: "#f9f0ff"
		})
	);

	$("#page-container").empty();

	if (loginState.user) {
		// User is logged in
		$("#page-container").append('<section id="journal-entries-section"></section>');

		JournalMainPage({ userId: loginState.user.id });

		$("#page-container").append(
			AddButton({
				type: "fab",
				color: "#1890ff",
				onclick: () => {
					showJournalFormModal(loginState.user.id);
				}
			})
		);
	} else {
		$("#page-container").append(
			LoginPage({
				onLoginButtonClick,
				onSignupButtonClick
			})
		);
	}

	$("#toolbar").append(Toolbar({ type: "bottom", title: "Journal", color: "#9254de" }));
};
