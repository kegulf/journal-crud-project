const db = require("../models");

module.exports = {
	list(req, res) {
		return db.JournalEntry.findAll()
			.then(journalEntries => res.status(200).send(journalEntries.reverse()))
			.catch(error => {
				res.status(400).send(error);
			});
	},

	getById(req, res) {
		return db.JournalEntry.findByPk(req.params.id)
			.then(journalEntry => {
				if (!journalEntry) {
					return res.status(404).send({
						message: "JournalEntry Not Found"
					});
				}
				return res.status(200).send(journalEntry);
			})
			.catch(error => res.status(400).send(error));
	},

	add(req, res) {
		console.log("running journalEntryController add function");
		console.log("request body: ", req.body);
		return db.JournalEntry.create({
			title: req.body.title,
			subtitle: req.body.subtitle,
			text: req.body.text,
			ownerId: req.body.ownerId
		})
			.then(journalEntry => res.status(201).send(journalEntry))
			.catch(error => res.status(400).send(error));
	},

	update(req, res) {
		return db.JournalEntry.findByPk(req.params.id)
			.then(journalEntry => {
				if (!journalEntry) {
					return res.status(404).send({
						message: "JournalEntry Not Found"
					});
				}
				return journalEntry
					.update({
						title: req.body.title,
						subtitle: req.body.subtitle,
						text: req.body.text
					})
					.then(() => res.status(200).send(journalEntry))
					.catch(error => res.status(400).send(error));
			})
			.catch(error => res.status(400).send(error));
	},

	delete(req, res) {
		return db.JournalEntry.findByPk(req.params.id)
			.then(journalEntry => {
				if (!journalEntry) {
					return res.status(400).send({
						message: "JournalEntry Not Found"
					});
				}
				return journalEntry
					.destroy()
					.then(() => res.status(204).send())
					.catch(error => res.status(400).send(error));
			})
			.catch(error => res.status(400).send(error));
	}
};
