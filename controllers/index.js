
const journalEntryController = require('./JournalEntryController');
const userController = require('./UserController');
const authController = require('./AuthController');


module.exports = {
  journalEntryController,
  userController,
  authController
};