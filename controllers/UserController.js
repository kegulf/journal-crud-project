const db = require("../models");

module.exports = {
	list(req, res) {
		return db.User.findAll()
			.then(users => res.status(200).send(users))
			.catch(error => {
				res.status(400).send(error);
			});
	},

	listJournalEntries(req, res) {
		const ownerId = req.params.userId;

		return db.JournalEntry.findAll({ where: { ownerId } })
			.then(entries => res.status(200).send(entries.reverse()))
			.catch(error => {
				res.status(400).send(error);
			});
	},

	getById(req, res) {
		return db.User.findByPk(req.params.id)
			.then(user => {
				if (!user) {
					return res.status(404).send({
						message: "User Not Found"
					});
				}
				return res.status(200).send(user);
			})
			.catch(error => res.status(400).send(error));
	},

	add(req, res) {
		return db.User.create({
			title: req.body.title,
			subtitle: req.body.subtitle,
			text: req.body.text
		})
			.then(user => res.status(201).send(user))
			.catch(error => res.status(400).send(error));
	},

	update(req, res) {
		return db.User.findByPk(req.params.id)
			.then(user => {
				if (!user) {
					return res.status(404).send({
						message: "User Not Found"
					});
				}
				return user
					.update({
						title: req.body.title,
						subtitle: req.body.subtitle,
						text: req.body.text
					})
					.then(() => res.status(200).send(user))
					.catch(error => res.status(400).send(error));
			})
			.catch(error => res.status(400).send(error));
	},

	delete(req, res) {
		return db.User.findByPk(req.params.id)
			.then(user => {
				if (!user) {
					return res.status(400).send({
						message: "User Not Found"
					});
				}
				return user
					.destroy()
					.then(() => res.status(204).send())
					.catch(error => res.status(400).send(error));
			})
			.catch(error => res.status(400).send(error));
	}
};
