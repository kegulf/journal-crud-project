var bcrypt = require("bcrypt"),
	db = require("../models"),
	_ = require("lodash"); // lodash import

module.exports = {
	signup(req, res) {
		const { name, email, password } = req.body;

		if (!name || !email || !password) {
			req.flash("error", "Please, fill in all the fields.");
			res.redirect("signup");
		}
		var salt = bcrypt.genSaltSync(10);
		var hashedPassword = bcrypt.hashSync(password, salt);

		var newUser = {
			name,
			email,
			password: hashedPassword
		};

		db.User.create(newUser)
			.then(result => {
				console.log("\n\nSTART\n\n");
				console.log(result.dataValues);

				console.log("\n\nEND\n\n");
				res.status(201).json({
					status: "ok",
					user: _.pick(result.dataValues, ["id", "name", "email", "createdAt", "modifiedAt"])
				});
			})
			.catch(function(error) {
				res.status(403).json({ error });
			});
	},

	async isAuthenticated(req, res, next) {
		if (req.isAuthenticated()) {
			res.status(200).json({
				status: "ok",
				user: _.pick(req.user, ["id", "name", "email", "createdAt", "modifiedAt"])
			});
		} else {
			res.status(403).json({ status: "error", message: "You failed bruv" });
		}
	},

	login(req, res) {
		//res.status(200).json({ status: "ok", user: req.user });

		const loggedInUser = req.user; // SOME MAGIC SHIT GOES HERE

		if (loggedInUser) {
			db.JournalEntry.findAll({
				where: { ownerId: loggedInUser.id }
			})
				.then(journalEntries =>
					res.status(200).send({
						status: "ok",
						user: loggedInUser,
						entries: journalEntries
					})
				)
				.catch(error => {
					res.status(400).send(error);
				});
		}
	},

	logout(req, res) {
		req.logout();
		res.redirect("/");
	}
};
